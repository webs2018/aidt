import { get, post } from './http';

/**
 * @param {Base} [Base Api]
 */
export const base = {
		/**
		 * 忘记密码
		 * @param {POST} Method [请求的类型]
		 */
		usernamezjc(res) {
			return post(process.env.API_HOST + 'user/usernamezjc', res);
		},
		/**
		 * 帐号登录
		 * @param {POST} Method [请求的类型]
		 */
		getSignUp(res) {
			return post(process.env.API_HOST + 'login', res);
		},
		/**
		 * 获取凭证
		 * @param {GET} Method [请求的类型]
		 */
		getCredentials(res) {
			return get(process.env.API_HOST + 'support/rsa', res);
		},
		/**
		 *  帐号注册,忘记密码的找回密码获取短信验证码
		 * @param {POST} Method [请求的类型]
		 */
		getCode(res) {
			return post(process.env.API_HOST + 'validate/code', res);
		},
		/**
		 *  获取注册协议
		 * @param {GET} Method [请求的类型]
		 */
		getRegisterDiscovers(res) {
			return get(process.env.API_HOST + 'web_config/configs/WEB_CONFIG_REGISTER_AGREEMENT', res);
		},
		/**
		 *  帐号注册
		 * @param {POST} Method [请求的类型]
		 */
		getRegister(res) {
			return post(process.env.API_HOST + 'user/registerByPhone', res);
		},
		/**
		 *  忘记密码的找回密码
		 * @param {POST} Method [请求的类型]
		 */
		getForgetPwd(res) {
			return post(process.env.API_HOST + 'user/forgotPasswordByPhone', res);
		},
		/**
		 *  获取本地应用资源版本号
		 * @param {GET} Method [请求的类型]
		 */
		getUpdate(res) {
			return get(process.env.API_HOST + 'apps/update.json', res);
		},
		/**
		 *  退出登录
		 * @param {POST} Method [请求的类型]
		 */
		getSignOut(res) {
			return post(process.env.API_HOST + 'user/logout', res);
		},
		/**
		 *  获取密保问题
		 * @param {POST} Method [请求的类型]
		 */
		getQuestions(res) {
			return post(process.env.API_HOST + 'user/getQuestions', res);
		},
		/**
		 *  根据用户获取密保问题
		 * @param {POST} Method [请求的类型]
		 */
		getAnswerByUserName(res) {
			return post(process.env.API_HOST + 'user/getAnswerByUserName', res);
		},
		/**
		 *  修改密保问题
		 * @param {POST} Method [请求的类型]
		 */
		getupdateSecretProtection(res) {
			return post(process.env.API_HOST + 'user/updateSecretProtection', res);
		},
		/**
		 *  实名认证
		 * @param {POST} Method [请求的类型]
		 */
		getReal_name(res) {
			return post(process.env.API_HOST + 'my/real_name', res);
		}
}

/**
 * @param {Home} [Home Api]
 */
export const home = {
		/**
		 * 首页轮播图
		 * @param {GET} Method [请求的类型]
		 */
		getBanner(res) {
			return get(process.env.API_HOST + 'web_config/configs/WEB_CONFIG_APP_BANNERS', res);
		},
		/**
		 * GET 获取最新公告列表，默认最新 5 条
		 * @param {GET} Method [请求的类型]
		 */
		getNoticesNewest(res) {
			return get(process.env.API_HOST + 'notices/newest', res);
		},
		/**
		 * GET 获取公告列表，首页的更多
		 * @param {GET} Method [请求的类型]
		 */
		getNotices(res) {
			return get(process.env.API_HOST + 'notices', res);
		},
		/**
		 * GET 获取神灯的参加记录
		 * @param {GET} Method [请求的类型]
		 */
		getJionRecord(res) {
			return get(process.env.API_HOST + 'lamp/jionRecord', res);
		},
		/**
		 * GET 获取超级节点的释放记录
		 * @param {GET} Method [请求的类型]
		 */
		getPlan(res) {
			return get(process.env.API_HOST + 'super/release', res);
		},
		/**
		 * GET 获取超级节点的下级奖励
		 * @param {GET} Method [请求的类型]
		 */
		getAward(res) {
			return get(process.env.API_HOST + 'super/award', res);
		},
		/**
		 * GET 获取超级节点的个人信息
		 * @param {GET} Method [请求的类型]
		 */
		getInfo(res) {
			return get(process.env.API_HOST + 'super/info', res);
		},
		/**
		 * GET 退出超级节点计划
		 * @param {GET} Method [请求的类型]
		 */
		getBack(res) {
			return get(process.env.API_HOST + 'super/back', res);
		},
		/**
		 * GET 获取阿拉丁的奖励信息
		 * @param {GET} Method [请求的类型]
		 */
		getAwardInfo(res) {
			return get(process.env.API_HOST + 'aladdin/awardInfo', res);
		},
		/**
		 * GET 获取阿拉丁的释放记录
		 * @param {GET} Method [请求的类型]
		 */
		getRecord(res) {
			return get(process.env.API_HOST + 'aladdin/record', res);
		},
		/**
		 * POST 获取神灯的释放记录
		 * @param {POST} Method [请求的类型]
		 */
		getReleaseRecord(res) {
			return post(process.env.API_HOST + 'lamp/releaseRecord', res);
		},
		/**
		 * GET 获取神灯的下级奖励
		 * @param {GET} Method [请求的类型]
		 */
		getFindNodeAward(res) {
			return get(process.env.API_HOST + 'lamp/findNodeAward', res);
		},
		/**
		 * GET 查询神灯计划最低买入
		 * @param {GET} Method [请求的类型]
		 */
		getMinJoinNum(res) {
			return get(process.env.API_HOST + 'lamp/minJoinNum', res);
		},
		/**
		 * GET 查询超级节点业绩
		 * @param {GET} Method [请求的类型]
		 */
		getPerformance(res) {
			return get(process.env.API_HOST + 'super/performance', res);
		},
		/**
		 * POST 获取K线详情数据
		 * @param {POST} Method [请求的类型]
		 */
		getKLine(res) {
			return post(process.env.API_HOST + 'indexs/klineTicker', res);
		},
		/**
		 * GET 客服
		 * @param {GET} Method [请求的类型]
		 */
		getCustomer(res) {
			return get(process.env.API_HOST + 'indexs/customer', res);
		},
}

/**
 * @param {Find} [Find Api]
 */
export const find = {
		/**
		 * 发现页面的内容
		 * @param {GET} Method [请求的类型]
		 */
		getDiscover(res) {
			return get(process.env.API_HOST + 'information', res);
		}
}

/**
 * @param {My} [My Api]
 */
export const mine = {
		/**
		 *  查询个人信息
		 * @param {GET} Method [请求的类型]
		 */
		getPersonal(res) {
			return get(process.env.API_HOST + 'user/user_info', res);
		},
		/**
		 *  查询个人账户信息
		 * @param {GET} Method [请求的类型]
		 */
		getAccounts(res) {
			return get(process.env.API_HOST + 'coin_accounts', res);
		},
		/**
		 *  查询是否有新消息
		 * @param {GET} Method [请求的类型]
		 */
		getIsRead(res) {
			return get(process.env.API_HOST + 'product/isRead', res);
		},
		/**
		 *  查询我的福利
		 * @param {GET} Method [请求的类型]
		 */
		getWelfare(res) {
			return get(process.env.API_HOST + 'account/welfare', res);
		},
		/**
		 *  查询签到状态
		 * @param {GET} Method [请求的类型]
		 */
		getSecond(res) {
			return get(process.env.API_HOST + 'admin/account/second', res);
		},
		/**
		 *  签到
		 * @param {GET} Method [请求的类型]
		 */
		getSign(res) {
			return get(process.env.API_HOST + 'admin/account/sign', res);
		},
		/**
		 *  初始化 websocket 之前需要查询的数据
		 * @param {GET} Method [请求的类型]
		 */
		getUserId(res) {
			return get(process.env.API_HOST + 'admin/message/userId', res);
		},
		/**
		 *  设置支付密码
		 * @param {POST} Method [请求的类型]
		 */
		getTradePassWord(res) {
			return post(process.env.API_HOST + 'user/setTradePassWord', res);
		},
		/**
		 *  修改支付密码
		 * @param {POST} Method [请求的类型]
		 */
		getUpdatePayPassword(res) {
			return post(process.env.API_HOST + 'user/upTradePassWrod', res);
		},
		/**
		 *  找回支付密码
		 * @param {POST} Method [请求的类型]
		 */
		getzhTradePassWrod(res) {
			return post(process.env.API_HOST + 'user/zhTradePassWrod', res);
		},
		/**
		 *  获取注册用户数量
		 * @param {GET} Method [请求的类型]
		 */
		getUsers(res) {
			return get(process.env.API_HOST + 'user/users', res);
		},
		/**
		 *  划转
		 * @param {POST} Method [请求的类型]
		 */
		getAccountTransfer(res) {
			return post(process.env.API_HOST + 'my/account_transfer', res);
		},
		/**
		 *  资产
		 * @param {GET} Method [请求的类型]
		 */
		getAsset(res) {
			return get(process.env.API_HOST + 'my/assets', res);
		},
		/**
		 *  账户
		 * @param {GET} Method [请求的类型]
		 */
		getWalletAccounts(res) {
			return get(process.env.API_HOST + 'my/wallet_accounts', res);
		},
		/**
		 *  我的团队-推广明细
		 * @param {GET} Method [请求的类型]
		 */
		getMyTeam(res) {
			return get(process.env.API_HOST + 'my/myteam', res);
		},
		/**
		 *  我的团队-收益明细
		 * @param {GET} Method [请求的类型]
		 */
		getRewardDetail(res) {
			return get(process.env.API_HOST + 'my/rewardDetail', res);
		},
		/**
		 *  我的糖果-赠送明细
		 * @param {GET} Method [请求的类型]
		 */
		getMyCandy(res) {
			return get(process.env.API_HOST + 'my/myCandy', res);
		},
		/**
		 *  我的糖果-释放明细
		 * @param {GET} Method [请求的类型]
		 */
		getDetails_release(res) {
			return get(process.env.API_HOST + 'my/details_release', res);
		},
		/**
		 *  总奖池
		 * @param {GET} Method [请求的类型]
		 */
		getPool(res) {
			return get(process.env.API_HOST + 'my/pool', res);
		}
}

/**
 * @param {ModifyPersonal} [ModifyPersonal Api]
 */
export const modifyPersonal = {
		/**
		 * 获取个人信息页面的设置信息
		 * @param {GET} Method [请求的类型]
		 */
		getModifyPersonal(res) {
			return get(process.env.API_HOST + 'admin/account/setting', res);
		},
		/**
		 * 获取完善资料的提示信息
		 * @param {GET} Method [请求的类型]
		 */
		getMaterial(res) {
			return get(process.env.API_HOST + 'admin/account/material', res);
		},
		/**
		 * 更新个人资料信息
		 * @param {POST} Method [请求的类型]
		 */
		getUpdate(res) {
			return post(process.env.API_HOST + 'admin/account/upsetting', res);
		}
}

/**
 * @param {Safety} [Safety Api]
 */
export const safety = {
		/**
		 * 安全设置页面判断支付密码
		 * @param {GET} Method [请求的类型]
		 */
		getTransaction(res) {
			return get(process.env.API_HOST + 'user/getIsTradePass', res);
		},
		/**
		 * 修改登录密码
		 * @param {POST} Method [请求的类型]
		 */
		getUpdatePassword(res) {
			return post(process.env.API_HOST + 'user/updatePassword', res);
		},
		/**
		 * 获取我的邀请码
		 * @param {GET} Method [请求的类型]
		 */
		getSharing(res) {
			return get(process.env.API_HOST + 'my/sharing', res);
		},
		/**
		 * 获取用户资金明细
		 * @param {GET} Method [请求的类型]
		 */
		getAccount_details(res) {
			return get(process.env.API_HOST + 'account_details', res);
		}
}

/**
 * @param {Change} [Change Api]
 */
export const change = {
		/**
		 * 获取转出手续费百分比
		 * @param {GET} Method [请求的类型]
		 */
		getRatio(res) {
			return get(process.env.API_HOST + 'user/transfer/ratio', res);
		},
		/**
		 * 输入密码返回助记词
		 * @param {POST} Method [请求的类型]
		 */
		passWrodZjc(res) {
			return post(process.env.API_HOST + 'user/passWrodZjc', res);
		},
		/**
		 * 个人中心转出按钮
		 * @param {POST} Method [请求的类型]
		 */
		getTransfer(res) {
			return post(process.env.API_HOST + 'user/transfer', res);
		}
}

/**
 * @param {SD,ALD,SuperJd} [SD,ALD,SuperJd Api]
 */
export const threeType = {
		/**
		 * 参加神灯计划
		 * @param {POST} Method [请求的类型]
		 */
		getJionPlan(res) {
			return post(process.env.API_HOST + 'lamp/jionPlan', res);
		},
		/**
		 * 参加超级节点
		 * @param {POST} Method [请求的类型]
		 */
		getSuperPlan(res) {
			return post(process.env.API_HOST + 'super/join', res);
		}
}

/**
 * @param {OTC} [OTC Api]
 */
export const otc = {
		/**
		 * OTC 列表
		 * @param {GET} Method [请求的类型]
		 */
		getAllOrder(res) {
			return get(process.env.API_HOST + 'otc/user/getAllOrder', res);
		},
		/**
		 * OTC 买入/卖出
		 * @param {POST} Method [请求的类型]
		 */
		getBuyOrSell(res) {
			return post(process.env.API_HOST + 'otc/user/buyOrSell', res);
		},
		/**
		 * OTC 我的订单
		 * @param {GET} Method [请求的类型]
		 */
		getOrder(res) {
			return get(process.env.API_HOST + 'otc/user/getOrder', res);
		},
		/**
		 * OTC 我的订单某一个订单的详细信息
		 * @param {GET} Method [请求的类型]
		 */
		getDetail(res) {
			return get(process.env.API_HOST + 'otc/user/getDetail', res);
		},
		/**
		 * OTC 商家订单
		 * @param {GET} Method [请求的类型]
		 */
		getMyOrder(res) {
			return get(process.env.API_HOST + 'otc/agent/myOrder', res);
		},
		/**
		 * OTC 查询商家订单支付方式
		 * @param {GET} Method [请求的类型]
		 */
		getPayType(res) {
			return get(process.env.API_HOST + 'payType/all', res);
		},
		/**
		 * OTC 查询商户支付方式
		 * @param {GET} Method [请求的类型]
		 */
		getOtcPayType(res) {
			return get(process.env.API_HOST + 'payType/getOtcPayType', res);
		},
		/**
		 * OTC 详情页面的更改用户订单状态-确认已付款
		 * @param {POST} Method [请求的类型]
		 */
		getUpdateDetailPayed(res) {
			return post(process.env.API_HOST + 'otc/updateDetailPayed', res);
		},
		/**
		 * OTC 详情页面的更改用户订单状态-确认已收款
		 * @param {POST} Method [请求的类型]
		 */
		getUpdateDetailConfirmReceipt(res) {
			return post(process.env.API_HOST + 'otc/updateDetailConfirmReceipt', res);
		},
		/**
		 * OTC 查询商家管理页的可用数量
		 * @param {POST} Method [请求的类型]
		 */
		getWallet_coin(res) {
			return post(process.env.API_HOST + 'my/wallet_coin', res);
		},
		/**
		 * OTC 配置信息查询
		 * @param {GET} Method [请求的类型]
		 */
		getOtcConfig(res) {
			return get(process.env.API_HOST + 'otc/getOtcConfig', res);
		},
		/**
		 * OTC 代理-发布订单
		 * @param {POST} Method [请求的类型]
		 */
		getCreate(res) {
			return post(process.env.API_HOST + 'otc/agent/create', res);
		},
		/**
		 * OTC 代理-取消订单
		 * @param {POST} Method [请求的类型]
		 */
		getCancel(res) {
			return post(process.env.API_HOST + 'otc/agent/cancel', res);
		},
		/**
		 * OTC 用户-取消订单
		 * @param {POST} Method [请求的类型]
		 */
		getUserCancelDetail(res) {
			return post(process.env.API_HOST + 'otc/user/userCancelDetail', res);
		},
		/**
		 * OTC 投诉
		 * @param {POST} Method [请求的类型]
		 */
		getAppealOrder(res) {
			return post(process.env.API_HOST + 'otc/appealOrder', res);
		},
		/**
		 * OTC 添加支付方式
		 * @param {POST} Method [请求的类型]
		 */
		getUpload(res) {
			return post(process.env.API_HOST + 'payType/upload', res);
		},
		/**
		 * OTC 流程查询
		 * @param {GET} Method [请求的类型]
		 */
		getProcess(res) {
			return get(process.env.API_HOST + 'otc/getProcess', res);
		},
		/**
		 * OTC 发送消息
		 * @param {POST} Method [请求的类型]
		 */
		addProcess(res) {
			return post(process.env.API_HOST + 'otc/addProcess', res);
		},
		/**
		 * OTC 查询是否已申诉
		 * @param {GET} Method [请求的类型]
		 */
		getIsAppeal(res) {
			return get(process.env.API_HOST + 'otc/getIsAppeal', res);
		}
}

/**
 * @param {币币交易} [币币交易 Api]
 */
export const money = {
	/**
	* 币币交易 买入卖出的数据的显示
	* @param {POST} Method [请求的类型]
	*/
	getPlatformBuyOrSell(res) {
		return post(process.env.API_HOST + 'trade/platformBuyOrSell', res);
	},
	/**
	* 币币交易 AIDT换算
	* @param {POST} Method [请求的类型]
	*/
	getPrice(res) {
		return post(process.env.API_HOST + 'trade/price', res);
	},
	/**
	* 币币交易 卖出
	* @param {POST} Method [请求的类型]
	*/
	getPay(res) {
		return post(process.env.API_HOST + 'trade/pay', res);
	},
	/**
	* 币币交易 一件买单委托
	* @param {POST} Method [请求的类型]
	*/
	getBuy(res) {
		return post(process.env.API_HOST + 'trade/buy', res);
	},
	/**
	* 币币交易 用户委托数据查询
	* @param {POST} Method [请求的类型]
	*/
	getMemberBuyOrSell(res) {
		return post(process.env.API_HOST + 'trade/memberBuyOrSell', res);
	},
	/**
	* 币币交易 用户委托数据查询-详情
	* @param {POST} Method [请求的类型]
	*/
	getMemberBuyOrSellInfo(res) {
		return post(process.env.API_HOST + 'trade/memberBuyOrSellInfo', res);
	}
}