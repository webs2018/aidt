// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import router from './router'
import App from './App'
import clipper from '../static/clipper'
Vue.use(clipper)

Vue.config.productionTip = false

import Vant from 'vant';
import 'vant/lib/index.css';
import '@/assets/styles/style.less'

// 视频组件
import VueVideoPlayer from 'vue-video-player'
import 'video.js/dist/video-js.css'
Vue.use(VueVideoPlayer)


//Vux框架组件全局引用
import {
	Group,
	Cell,
	XButton,
	XInput,
	Toast,
	Grid,
	GridItem,
	GroupTitle,
	Confirm,
	Flexbox, 
	FlexboxItem 
} from 'vux'
Vue.component('group', Group)
Vue.component('cell', Cell)
Vue.component('XButton', XButton)
Vue.component('XInput', XInput)
Vue.component('Toast', Toast)
Vue.component('Grid', Grid)
Vue.component('GridItem', GridItem)
Vue.component('GroupTitle', GroupTitle)
Vue.component('confirm', Confirm)
Vue.component('flexbox', Flexbox)
Vue.component('flexbox-item', FlexboxItem)

import  { ConfirmPlugin } from 'vux'
Vue.use(ConfirmPlugin)

// vux 的 Toast 窗组件
import {
	ToastPlugin
} from 'vux'
Vue.use(ToastPlugin)

// 二维码组件
import VueQriously from 'vue-qriously'
Vue.use(VueQriously)

// axios 框架
import axios from 'axios'
import Qs from 'qs'
Vue.prototype.axios = axios;
Vue.prototype.qs = Qs;

// 挂载 vant 组件
Vue.use(Vant);

// 全局函数
Vue.prototype.$develop = function() {
	this.$vux.toast.text('正在开发中,敬请期待!');
}

// 统一的提示层组件
import { LoadingPlugin } from 'vux';
Vue.use(LoadingPlugin);
import { Notify } from 'vant';
Vue.use(Notify);

// 判断是否实名验证及设置支付密码
Vue.prototype.$Permission = function(path) {
	// 验证是否已实名
	this.axios.get(process.env.API_HOST+'account/my/detail').then(res => { 
		if(res.data.code == 200){
			if(res.data.data.statusId == 1 ){
				// 当已实名在判断是否设置支付密码
				this.axios.get(process.env.API_HOST+'wallet/user/tradePass').then(res => { // 判断是还是设置支付密码
					if(res.data.data) {  //当true为已设置支付密码
						this.$router.push({
							path
						});
					}else{
						let _this = this
						this.$vux.confirm.show({
							title:'支付密码',
							content: '请先去设置支付密码？',
							onConfirm () {
								_this.$router.push({
									path: '/UpdatePayPwd?type=1'
								}); 
							}
						})
					}
				})
			}else{
				let _this = this
				this.$vux.confirm.show({
					title:'实名认证',
					content: '未完成实名认证，请完成实名认证！',
					onConfirm () {
						sessionStorage.Certification = res.data.data.statusId
						sessionStorage.remark = res.data.data.remark
						_this.$router.push({
							path: '/Verified'
						}); 
					}
				})
			}
		}
	})
}
let _this;
import { Dialog } from 'vant';
// 判断是否设置支付密码
Vue.prototype.$PayPwd = function(path,path2) {
	this.axios.get(process.env.API_HOST+'user/getIsTradePass').then(res => { // 判断是还是设置支付密码
		if(res.data.data == 1){  // 当true为已设置支付密码
			this.$router.push({
				path
			});
		}else{
			let _this = this
			Dialog.confirm({
				message: '您未设置支付密码，请设置支付密码！',
				confirmButtonText: '立即设置',
				cancelButtonText: '稍后设置',
				cancelButtonColor: 'white'
			}).then(() => {
				_this.$router.push({
					path: '/UpdatePayPwd',
					query: {
						type: 0,
						roid: path2
					}
				})
			}).catch(() => {  //稍后设置
			});
		}
		// if(res.data.data) {  // 当true为已设置支付密码
		// 	this.$router.push({
		// 		path
		// 	});
		// }else{
		// 	let _this = this
		// 	this.$vux.confirm.show({
		// 		title:'支付密码',
		// 		content: '请先去设置支付密码？',
		// 		onConfirm () {
		// 			_this.$router.push({
		// 				path: 'UpdatePayPwd?type=0'
		// 			}); 
		// 		}
		// 	})
		// }
	})
}

// 页面权限
router.beforeEach((to, from, next) => {
	if (to.matched.some(item => item.meta.requiresAuth)) {
		// 当 token 存在证已经登录，设置登录后的请求头，否则进入登录页面
		if(sessionStorage.token){
			axios.defaults.headers['Authorization'] = 'bearer ' + sessionStorage.token;
			next();
		}else{
			next({
				path: '/Login'
			});
		}
	} else {
		if(to.path == '/Register' || to.path == '/FindPwd' || to.path == '/Login'){
			axios.defaults.headers['Authorization'] = '';
		}
		next();
	}
	// 响应拦截（配置请求回来的信息）
	axios.interceptors.response.use(function(response) { // 处理响应数据
		// 判断如果请求返回1004 3301 4004 5501状态码，即登录超时，清除token并跳回登录页
		if (response.data.code == '1004' || response.data.code == '3301' || response.data.code == '5501') {
			Notify({
				message: response.data.msg,
				duration: 1000,
				background: '#1989fa'
			});
			sessionStorage.clear();
			next({
				path: '/Login'
			});
		}
		return response;
	}, function(error) { // 处理响应失败
		setTimeout(() => {
			if(error.response.status == '404') {
				Notify({
					message: '404 请求接口地址错误',
					duration: 1000,
					background: '#1989fa'
				});
			}else if(error.response.status == '405') {
				Notify({
					message: '405 请求类型错误',
					duration: 1000,
					background: '#1989fa'
				});
			}else if(error.response.status == '500') {
				Notify({
					message: error.response.data.msg,
					duration: 1000,
					background: 'rgb(255, 68, 68)'
				});
			}else if(error.response.status == '504') {
				Notify({
					message: '504 网关错误',
					duration: 1000,
					background: 'rgb(255, 68, 68)'
				});
			}else{
				Notify({
					message: error.response.status + ' 其他错误：' + error.response.data.msg,
					duration: 1000,
					background: 'rgb(255, 68, 68)'
				});
			}
		}, 1000);
		return Promise.reject(error);
	});
});

// 加密
// 正常加密
Vue.prototype.rsaEncrypt = function(text) {
	let pubkey =
		"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCxnhv0xsWaLUc+WZleQFHznWbDUeWHeRVH9lClgm5SVUjCAGuD1GZHZlrojMrhwG65eeYM1NtmhZ96nwzL2OV5SgxFgEscHjbExdvVNckw4mYUlyL8QyhnSEQvgHUokcQcBt9oJZMajnmwyDbPTFOUxmTnongFksrbTeJf6XUW5QIDAQAB";
	// java后台生成的
	let encrypt = new JSEncrypt();
	encrypt.setPublicKey(pubkey);
	// 加密
	return encrypt.encrypt(text)
};
// 超长加密
Vue.prototype.encryptLong = function(text) {
	let pubkey =
		"MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCxnhv0xsWaLUc+WZleQFHznWbDUeWHeRVH9lClgm5SVUjCAGuD1GZHZlrojMrhwG65eeYM1NtmhZ96nwzL2OV5SgxFgEscHjbExdvVNckw4mYUlyL8QyhnSEQvgHUokcQcBt9oJZMajnmwyDbPTFOUxmTnongFksrbTeJf6XUW5QIDAQAB";
	// java后台生成的
	let encrypt = new JSEncrypt();
	encrypt.setPublicKey(pubkey);
	//加密
	return encrypt.encryptLong(text)
};


/* eslint-disable no-new */
new Vue({
  router,
  render: h => h(App)
}).$mount('#app-box')
