import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

export default new Router({
	linkActiveClass: 'on',
	routes: [{
		path: '/',
		component: () =>
			import('@/components/Index'),
		children: [{
			path: '',
			name: 'Home',
			component: () =>
				import('@/components/Home'),
			meta: {
				title: '首页'
			}
		},
		{
			path: '/Transaction',
			name: 'Transaction',
			component: () =>
				import('@/components/Transaction'),
			meta: {
				title: '交易'
			}
		}, {
			path: '/Otc',
			name: 'Otc',
			component: () =>
				import('@/components/Otc'),
			meta: {
				title: 'OTC'
			}
		},
		{
			path: '/find',
			name: 'find',
			component: () =>
				import('@/components/find'),
			meta: {
				title: '发现'
			}
		},
		{
			path: '/Assets',
			name: 'Assets',
			component: () =>
				import('@/components/Assets'),
			meta: {
				requiresAuth: true,
				title: '我的'
			}
		}]
	}, {
		path: '/Login',
		name: 'Login',
		component: () =>
			import('@/components/Login'),
		meta: {
			title: '登录'
		}
	}, {
		path: '/Register',
		name: 'Register',
		component: () =>
			import('@/components/Register'),
		meta: {
			title: '注册'
		}
	}, {
		path: '/Qr',
		name: 'Qr',
		component: () =>
			import('@/components/Qr'),
		meta: {
			title: '扫一扫'
		}
	}, {
		path: '/FindPwd',
		name: 'FindPwd',
		component: () =>
			import('@/components/FindPwd'),
		meta: {
			title: '忘记密码'
		}
	}, {
		path: '/NewsDetails',
		name: 'NewsDetails',
		component: () =>
			import('@/components/NewsDetails'),
		meta: {
			title: '公告详情'
		}
	}, {
		path: '/Setting',
		name: 'Setting',
		component: () =>
			import('@/components/Setting'),
		meta: {
			requiresAuth: true,
			title: '设置'
		}
	}, {
		path: '/ModifyPersonal',
		name: 'ModifyPersonal',
		component: () =>
			import('@/components/ModifyPersonal'),
		meta: {
			requiresAuth: true,
			title: '个人信息'
		}
	}, {
		path: '/Safety',
		name: 'Safety',
		component: () =>
			import('@/components/Safety'),
		meta: {
			requiresAuth: true,
			title: '安全设置'
		}
	}, {
		path: '/ToChange',
		name: 'ToChange',
		component: () =>
			import('@/components/ToChange'),
		meta: {
			requiresAuth: true,
			title: '转入'
		}
	}, {
		path: '/OutChange',
		name: 'OutChange',
		component: () =>
			import('@/components/OutChange'),
		meta: {
			requiresAuth: true,
			title: '转出'
		}
	}, {
		path: '/Funds',
		name: 'Funds',
		component: () =>
			import('@/components/Funds'),
		meta: {
			requiresAuth: true,
			title: '资金明细'
		}
	}, {
		path: '/Kf',
		name: 'Kf',
		component: () =>
			import('@/components/Kf'),
		meta: {
			requiresAuth: true,
			title: '客服'
		}
	}, {
		path: '/Kf2',
		name: 'Kf2',
		component: () =>
			import('@/components/Kf2'),
		meta: {
			requiresAuth: true,
			title: '留言成功'
		}
	}, {
		path: '/Member',
		name: 'Member',
		component: () =>
			import('@/components/Member'),
		meta: {
			requiresAuth: true,
			title: '邀请列表'
		}
	}, {
		path: '/Invite',
		name: 'Invite',
		component: () =>
			import('@/components/Invite'),
		meta: {
			requiresAuth: true,
			title: '分享注册'
		}
	}, {
		path: '/UpdatePwd',
		name: 'UpdatePwd',
		component: () =>
			import('@/components/UpdatePwd'),
		meta: {
			requiresAuth: true,
			title: '修改登录密码'
		}
	}, {
		path: '/UpdatePayPwd',
		name: 'UpdatePayPwd',
		component: () =>
			import('@/components/UpdatePayPwd'),
		meta: {
			requiresAuth: true,
			title: '修改支付密码'
		}
	}, {
		path: '/UpdateMb',
		name: 'UpdateMb',
		component: () =>
			import('@/components/UpdateMb'),
		meta: {
			requiresAuth: true,
			title: '修改支付密码'
		}
	}, {
		path: '/Authentication',
		name: 'Authentication',
		component: () =>
			import('@/components/Authentication'),
		meta: {
			requiresAuth: true,
			title: '实名认证'
		}
	}, {
		path: '/NewsList',
		name: 'NewsList',
		component: () =>
			import('@/components/NewsList'),
		meta: {
			title: '公告列表'
		}
	}, {
		path: '/NewsDetails2',
		name: 'NewsDetails2',
		component: () =>
			import('@/components/NewsDetails2'),
		meta: {
			title: '公告详情'
		}
	}, {
		path: '/Four',
		name: 'Four',
		component: () =>
			import('@/components/Four'),
		meta: {
			requiresAuth: true,
			title: 'Four'
		}
	}, {
		path: '/Explain',
		name: 'Explain',
		component: () =>
			import('@/components/Explain'),
		meta: {
			requiresAuth: true,
			title: '规则说明'
		}
	}, {
		path: '/My',
		name: 'My',
		component: () =>
			import('@/components/My'),
		meta: {
			requiresAuth: true,
			title: '个人中心'
		}
	}, {
		path: '/MerchantOrder',
		name: 'MerchantOrder',
		component: () =>
			import('@/components/MerchantOrder'),
		meta: {
			requiresAuth: true,
			title: '商家订单'
		}
	}, {
		path: '/OrderRecord',
		name: 'OrderRecord',
		component: () =>
			import('@/components/OrderRecord'),
		meta: {
			requiresAuth: true,
			title: '订单管理'
		}
	}, {
		path: '/prePayment',
		name: 'prePayment',
		component: () =>
			import('@/components/prePayment'),
		meta: {
			requiresAuth: true,
			title: '待付款'
		}
	}, {
		path: '/prePayment2',
		name: 'prePayment2',
		component: () =>
			import('@/components/prePayment2'),
		meta: {
			requiresAuth: true,
			title: '待付款-备用'
		}
	}, {
		path: '/KLine',
		name: 'KLine',
		component: () =>
			import('@/components/KLine'),
		meta: {
			title: 'K线图'
		}
	}, {
		path: '/Pay',
		name: 'Pay',
		component: () =>
			import('@/components/Pay'),
		meta: {
			requiresAuth: true,
			title: '支付账户'
		}
	}, {
		path: '/ZFB',
		name: 'ZFB',
		component: () =>
			import('@/components/ZFB'),
		meta: {
			requiresAuth: true,
			title: '添加支付宝'
		}
	}, {
		path: '/WX',
		name: 'WX',
		component: () =>
			import('@/components/WX'),
		meta: {
			requiresAuth: true,
			title: '添加微信'
		}
	}, {
		path: '/YHK',
		name: 'YHK',
		component: () =>
			import('@/components/YHK'),
		meta: {
			requiresAuth: true,
			title: '添加银行卡'
		}
	}, {
		path: '/MyTeam',
		name: 'MyTeam',
		component: () =>
			import('@/components/MyTeam'),
		meta: {
			requiresAuth: true,
			title: '我的团队'
		}
	}, {
		path: '/MyCandy',
		name: 'MyCandy',
		component: () =>
			import('@/components/MyCandy'),
		meta: {
			requiresAuth: true,
			title: '我的糖果'
		}
	}, {
		path: '/Transfer',
		name: 'Transfer',
		component: () =>
			import('@/components/Transfer'),
		meta: {
			requiresAuth: true,
			title: '划转'
		}
	}, {
		path: '/Authentication2',
		name: 'Authentication2',
		component: () =>
			import('@/components/Authentication2'),
		meta: {
			requiresAuth: true,
			title: '身份认证'
		}
	}, {
		path: '/Invite2',
		name: 'Invite2',
		component: () =>
			import('@/components/Invite2'),
		meta: {
			requiresAuth: true,
			title: '我的邀请码'
		}
	}, {
		path: '/AuthSuccess',
		name: 'AuthSuccess',
		component: () =>
			import('@/components/AuthSuccess'),
		meta: {
			requiresAuth: true,
			title: '认证成功'
		}
	}, {
		path: '/UserCenter',
		name: 'UserCenter',
		component: () =>
			import('@/components/UserCenter'),
		meta: {
			requiresAuth: true,
			title: '个人中心'
		}
	}, {
		path: '/TS',
		name: 'TS',
		component: () =>
			import('@/components/TS'),
		meta: {
			requiresAuth: true,
			title: '投诉'
		}
	}, {
			path: '/TransactionInfo',
			name: 'TransactionInfo',
			component: () =>
				import('@/components/TransactionInfo'),
			meta: {
				title: '交易详情'
			}
		}]
})